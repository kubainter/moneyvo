<?php

namespace Unit;

use Faker\Factory;
use Money\Currency;
use PHPUnit\Framework\TestCase;

class NewCurrencyTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    public function testCanApplyUppercase(): void
    {
        /** always return lowercase currency */
        $currency = $this->faker->currencyCode();

        $this->assertEquals($currency, (new Currency(mb_strtolower($currency)))->getCode());
    }
}
