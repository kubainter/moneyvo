<?php

namespace Unit;

use Faker\Factory;
use Money\Currency;
use InvalidArgumentException;
use Money\Exception\InvalidAmountException;
use Money\Money;
use PHPUnit\Framework\TestCase;
use TypeError;

class NewMoneyTest extends TestCase
{
    const MAX_DECIMALS = 2;
    /**
     * @var \Faker\Generator
     */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    public function testIsObjectCreatedCorrectly()
    {
        $currency = new Currency($this->faker->currencyCode());
        $amount   = $this->faker->randomFloat(self::MAX_DECIMALS);
        $money    = new Money($amount, $currency);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals(new Money($amount, $currency), $money);

    }

    public function testCanGetCurrency()
    {
        $currency = new Currency($this->faker->currencyCode());
        $money    = new Money($this->faker->randomFloat(self::MAX_DECIMALS), $currency);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($money->getCurrency()->getCode(), $currency->getCode());
    }

    public function testCanGetAmount()
    {
        $amount = $this->faker->randomFloat(self::MAX_DECIMALS);
        $money  = new Money($amount, new Currency($this->faker->currencyCode()));

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($money->getAmount(), $amount);
    }

    public function testCanAddAmount()
    {
        $currency   = new Currency($this->faker->currencyCode());
        $money      = new Money($this->faker->randomFloat(self::MAX_DECIMALS), $currency);
        $moneyToAdd = new Money($this->faker->randomFloat(self::MAX_DECIMALS), $currency);

        $expected = round($money->getAmount() + $moneyToAdd->getAmount(), 2);
        $sumMoney = $money->add($moneyToAdd);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $sumMoney->getAmount());
    }

    public function testCanSubtractAmount()
    {
        $currency   = new Currency($this->faker->currencyCode());
        $money      = new Money($this->faker->randomFloat(self::MAX_DECIMALS), $currency);
        $moneyToSub = new Money($this->faker->randomFloat(self::MAX_DECIMALS, 0, $money->getAmount()), $currency);

        $expected = round($money->getAmount() - $moneyToSub->getAmount(), 2);
        $sumMoney = $money->subtract($moneyToSub);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $sumMoney->getAmount());
    }

    public function testCanMultipliesTheAmount(): void
    {
        $money = new Money($this->faker->randomFloat(self::MAX_DECIMALS), new Currency($this->faker->currencyCode()));

        $multiplier = 8;
        $expected   = round($money->getAmount() * $multiplier, 2);
        $money      = $money->multiply($multiplier);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $money->getAmount());
    }

    public function testCanMultipliesTheAmountWithCommaSeparatorValue(): void
    {
        $money = new Money($this->faker->randomFloat(self::MAX_DECIMALS), new Currency($this->faker->currencyCode()));

        $multiplier = 0.1;
        $expected   = round($money->getAmount() * $multiplier, 2);
        $money      = $money->multiply($multiplier);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $money->getAmount());
    }

    public function testCanDivideTheAmount(): void
    {
        $money = new Money($this->faker->randomFloat(self::MAX_DECIMALS), new Currency($this->faker->currencyCode()));

        $divider  = 3;
        $expected = round($money->getAmount() / $divider, 2);
        $money    = $money->divide($divider);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $money->getAmount());
    }

    public function testCanDivideTheAmountWithCommaSeparatorValue(): void
    {
        $money = new Money($this->faker->randomFloat(self::MAX_DECIMALS), new Currency($this->faker->currencyCode()));

        $divider  = 0.9;
        $expected = round($money->getAmount() / $divider, 2);
        $money    = $money->divide($divider);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals($expected, $money->getAmount());
    }

    public function testItThrowExceptionForCalculatingTwoDifferentCurrency()
    {
        $money = new Money($this->faker->randomFloat(self::MAX_DECIMALS), new Currency($this->faker->currencyCode()));

        $newCurrency            = $this->faker->currencyCode();
        $moneyWithOtherCurrency = new Money(
            $this->faker->randomFloat(self::MAX_DECIMALS),
            new Currency($newCurrency == $money->getCurrency()->getCode() ? $this->faker->currencyCode() : $newCurrency)
        );

        $this->expectException(InvalidArgumentException::class);

        $money->add($moneyWithOtherCurrency);
    }

    public function testItThrowExceptionForSettingWrongAmount()
    {
        $this->expectException(InvalidAmountException::class);

        new Money($this->faker->numberBetween(-999999, -1), new Currency($this->faker->currencyCode()));
    }

    public function testItThrowExceptionForSettingWrongFormat()
    {
        $this->expectException(TypeError::class);

        new Money($this->faker->randomLetter(), new Currency($this->faker->currencyCode()));
    }
}
