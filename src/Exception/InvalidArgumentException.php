<?php
declare(strict_types=1);

namespace Money\Exception;

use InvalidArgumentException as MainInvalidArgumentException;

final class InvalidArgumentException extends MainInvalidArgumentException
{

}
