<?php
declare(strict_types=1);

namespace Money\Exception;

use Money\Exception\Currency\CurrencyException;

final class InvalidCurrencyCodeException extends CurrencyException
{
    public static function withAllowedTypes(string $provided, array $allowed): self
    {
        $allowed = implode(", ", $allowed);

        return new static("Provided currency is not allowed (allowed: '$allowed', provided: $provided)");
    }
}
