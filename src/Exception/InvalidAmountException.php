<?php
declare(strict_types=1);

namespace Money\Exception;

use Money\Exception\Amount\AmountException;

final class InvalidAmountException extends AmountException
{

}
