<?php
declare(strict_types=1);

namespace Money;

use InvalidArgumentException;
use Money\Exception\InvalidAmountException;

class Money
{
    /** @var float */
    private $amount;

    /** @var Calculator */
    public $calculator;

    /** @var Currency */
    private $currency;

    public function __construct(float $amount, Currency $currency)
    {
        $this->assertValidValue($amount);

        $this->amount     = round($amount, 2);
        $this->currency   = $currency;
        $this->calculator = new Calculator();
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function add(Money $addMoney): Money
    {
        $this->compareCurrencies($addMoney);

        $amount = $this->calculator->add($this->amount, $addMoney->getAmount());

        return new self($amount, $this->currency);
    }

    public function subtract(Money $subMoney): Money
    {
        $this->compareCurrencies($subMoney);

        $amount = $this->calculator->sub($this->amount, $subMoney->getAmount());

        return new self($amount, $this->currency);
    }

    public function multiply(float $multiplier): Money
    {
        $value = $this->calculator->multiply($this->amount, $multiplier);

        return new self($value, $this->currency);
    }

    public function divide(float $divisor): Money
    {
        $value = $this->calculator->divide($this->amount, $divisor);

        return new self($value, $this->currency);
    }

    private function compareCurrencies($compareObject)
    {
        if ($this->currency != $compareObject->currency) {
            throw new InvalidArgumentException('Currencies must be identical');
        }
    }

    private function assertValidValue(float $amount)
    {
        if ($amount >= 0) {
            return true;
        }

        throw new InvalidAmountException('Amount must be greater than zero');
    }
}
