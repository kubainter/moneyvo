<?php
declare(strict_types=1);

namespace Money;

class Calculator
{
    const ROUND_PRECISION = 2;

    public function add(float $amount, float $addValue): float
    {
        return round($amount + $addValue, self::ROUND_PRECISION);
    }

    public function sub(float $amount, float $subValue): float
    {
        return round($amount - $subValue, self::ROUND_PRECISION);
    }

    public function multiply(float $amount, float $multiplier): float
    {
        return round($amount * $multiplier, self::ROUND_PRECISION);
    }

    public function divide(float $amount, float $divider): float
    {
        return round($amount / $divider, self::ROUND_PRECISION);
    }
}
